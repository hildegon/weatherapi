Feature: GET in weather API

  Scenario: Send a GET to know how is the weather in any city by name
    Given I send a GET request to the service "weather" to get the weather at the city "Zaragoza"
    Then the response should return status code "200"
    And the value for "name" should be "Zaragoza"
    And the value for "sys.country" should be "ES"

  Scenario: Send a GET to know how is the weather in any city by city Id
    Given I send a GET request to the service "weather" to get the weather at the city "3104324"
    Then the response should return status code "200"
    And the value for "name" should be "Zaragoza"
    And the value for "sys.country" should be "ES"

  Scenario: Send a GET to know how is the weather in any city by zip code
    Given I send a GET request to the service "weather" to get the weather at the city "50001,es"
    Then the response should return status code "200"
    And the value for "name" should be "Zaragoza"
    And the value for "sys.country" should be "ES"

  Scenario: Send a GET to know how is the weather in any city by lat and long
    Given I send a GET request to the service "weather" with the parameters:
      | lat | 41.66 |
      | lon | -0.88 |
    Then the response should return status code "200"
    And the value for "name" should be "Zaragoza"
    And the value for "sys.country" should be "ES"

  Scenario: Send a GET with zip code but without country specification
    Given I send a GET request to the service "weather" to get the weather at the city "50001"
    Then the response should return status code "200"
    And the value for "name" should be "Des Moines"
    And the value for "sys.country" should be "US"

  Scenario: Send a GET to a non existing city
    Given I send a GET request to the service "weather" to get the weather at the city "azeroth"
    Then the response should return status code "404"
    And the value for "message" should be "city not found"

  Scenario: Send a GET with only lon coords
    Given I send a GET request to the service "weather" with the parameters:
      | lon | -0.88 |
    Then the response should return status code "400"
    And the value for "message" should be "Nothing to geocode"

#    -------------------------------------------------------------------------------------------------------------------

  Scenario: GET current weather data for several cities by id
    Given I send a GET request to the service "group" with the parameters:
      | id | 524901,703448,2643743 |
    Then the response should return status code "200"
    And the value for "list.name" should contain the values:
      | London |
      | Kiev   |
      | Moscow |
    And the value for "list.find{list -> list.name == 'London'}.id" should be equal than 2643743
    And the value for "list.find{list -> list.name == 'Kiev'}.id" should be equal than 703448
    And the value for "list.find{list -> list.name == 'Moscow'}.id" should be equal than 524901

  Scenario: GET current weather data for several cities by id with one id with wrong dataType
    Given I send a GET request to the service "group" with the parameters:
      | id | 524901,703448,Zaragoza |
    Then the response should return status code "400"
    And the value for "message" should be "Zaragoza is not a city id"

  Scenario: GET current weather data for several cities by id with id unexistent
    Given I send a GET request to the service "group" with the parameters:
      | id | 524901,703448,2643743475698446 |
    Then the response should return status code "404"
    And the value for "message" should be "City not found: 2643743475698446"

  Scenario: Weather GET Cities within a rectangle zone
    Given I send a GET request to the service "box/city" with the parameters:
      | bbox | 12,32,15,37,7 |
    Then the response should return status code "200"
    And the value for "cnt" should be equal than 3
    Then the value for "list.find{list -> list.name == 'Zawiya'}.coord.Lat" should be GTE than 32
    Then the value for "list.find{list -> list.name == 'Tripoli'}.coord.Lat" should be GTE than 32
    Then the value for "list.find{list -> list.name == 'Ragusa'}.coord.Lat" should be GTE than 32
    Then the value for "list.find{list -> list.name == 'Zawiya'}.coord.Lat" should be LTE than 37
    Then the value for "list.find{list -> list.name == 'Tripoli'}.coord.Lat" should be LTE than 37
    Then the value for "list.find{list -> list.name == 'Ragusa'}.coord.Lat" should be LTE than 37
    Then the value for "list.find{list -> list.name == 'Zawiya'}.coord.Lon" should be GTE than 12
    Then the value for "list.find{list -> list.name == 'Tripoli'}.coord.Lon" should be GTE than 12
    Then the value for "list.find{list -> list.name == 'Ragusa'}.coord.Lon" should be GTE than 12
    Then the value for "list.find{list -> list.name == 'Zawiya'}.coord.Lon" should be LTE than 15
    Then the value for "list.find{list -> list.name == 'Tripoli'}.coord.Lon" should be LTE than 15
    Then the value for "list.find{list -> list.name == 'Ragusa'}.coord.Lon" should be LTE than 15
    And the value for "list.name" should contain the values:
      | Zawiya  |
      | Tripoli |
      | Ragusa  |

  Scenario: Weather GET Cities within a rectangle zone with wrong dataType in request
    Given I send a GET request to the service "box/city" with the parameters:
      | bbox | 12,unexisten,15,37,7 |
    Then the response should return status code "400"
    And the value for "message" should be "\"unexisten\" is not a float"

  Scenario: Weather GET Cities within a rectangle zone with 0 zoom
    Given I send a GET request to the service "box/city" with the parameters:
      | bbox | 12,32,15,37,0 |
    Then the response should return status code "200"
    And the value for "" should not contain the values:
      | cnt |

  Scenario: Weather GET Cities within a rectangle zone with one left parameter
    Given I send a GET request to the service "box/city" with the parameters:
      | bbox | 12,15,37 |
    Then the response should return status code "400"
    And the value for "message" should be "Bbox is required"

  Scenario: Weather GET Cities in cycle
    Given I send a GET request to the service "find" with the parameters:
      | lat | 55.5 |
      | lon | 37   |
      | cnt | 2    |
    Then the response should return status code "200"
    And the value for "list.find{list -> list.name == 'Selyatino'}.coord.lat" should be GTE than 55
    And the value for "list.find{list -> list.name == 'Alabino'}.coord.lat" should be GTE than 55
    And the value for "count" should be equal than 2
    And the value for "list.name" should contain the values:
      | Selyatino |
      | Alabino   |


  Scenario: Weather GET Cities in cycle default number of cities
    Given I send a GET request to the service "find" with the parameters:
      | lat | 55.5 |
      | lon | 37   |
    Then the response should return status code "200"
    And the value for "count" should be equal than 5

  Scenario: Weather GET Cities in cycle with one parameter left
    Given I send a GET request to the service "find" with the parameters:
      | lon | 37 |
      | cnt | 2  |
    Then the response should return status code "400"
    And the value for "message" should be "Nothing to geocode"
