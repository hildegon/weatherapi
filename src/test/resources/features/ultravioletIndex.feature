
Feature: GET Ultraviolet index

  Scenario: GET current UV data for one location
    Given I send a GET request to the service "uvi" with the parameters:
      | lat | 41.66 |
      | lon | -0.88 |
    Then the response should return status code "200"
    And the value for "value" shouldn't be empty
    And the value for "value" should be GTE than 0

  Scenario: GET forecast UV data for one location
    Given I send a GET request to the service "uvi/forecast" with the parameters:
      | lat | 41.66 |
      | lon | -0.88 |
    Then the response should return status code "200"
    And the value for "value" shouldn't be empty
    And the JSON array located on "" should contain 7 items
    And the date for "date_iso[1]" should be LT than "date_iso[2]"
    And the value for "value[3]" should be GTE than 0

  Scenario: GET historical UV data for one location
    Given I send a GET request to the service "uvi/history" with the parameters:
      | lat   | 41.66      |
      | lon   | -0.88      |
      | start | 1498049953 |
      | end   | 1498481991 |
    Then the response should return status code "200"
    And the value for "date[0]" should be LT than 1498481991
    And the value for "date[0]" should be GT than 1498049953

  Scenario: GET historical UV data for one location with a date before historical data
    Given I send a GET request to the service "uvi/history" with the parameters:
      | lat   | 41.66      |
      | lon   | -0.88      |
      | start | -946771200 |
      | end   | -915148800 |
    Then the response should return status code "200"
    And the JSON array located on "" should contain 0 items

  Scenario: GET current UV data for one wrong location
    Given I send a GET request to the service "uvi" with the parameters:
      | lat | 65862185.66     |
      | lon | 567651856524.88 |
    Then the response should return status code "400"
    And the response should return code "Lat field is not valid.*max 90"

