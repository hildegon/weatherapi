Feature: Weather forecast

  Scenario: GET current weather forecast for a city by name
    Given I send a GET request to the service "forecast" to get the weather at the city "Zaragoza,es"
    Then the response should return status code "200"
    And the value for "list" shouldn't be empty
    And the value for "city.name" should be "Zaragoza"
    And the value for "city.country" should be "ES"
    And the date for "list[1].dt_txt" should be LT than "list[2].dt_txt"
    And the value for "cnt" should be GT than 5

  Scenario: GET current weather forecast for a city by unexistent name
    Given I send a GET request to the service "forecast" to get the weather at the city "Ventormenta,az"
    Then the response should return status code "404"
    And the value for "message" should be "city not found"

  Scenario: GET current weather forecast for a city by id
    Given I send a GET request to the service "forecast" to get the weather at the city "3104324"
    Then the response should return status code "200"
    And the value for "list" shouldn't be empty
    And the value for "city.name" should be "Zaragoza"
    And the value for "city.country" should be "ES"
    And the date for "list[1].dt_txt" should be LT than "list[2].dt_txt"
    And the value for "cnt" should be GT than 5

  Scenario: GET current weather forecast for a city by unexistent id
    Given I send a GET request to the service "forecast" to get the weather at the city "33333333"
    Then the response should return status code "404"
    And the value for "message" should be "city not found"

  Scenario: GET current weather forecast for a city by zip code
    Given I send a GET request to the service "forecast" to get the weather at the city "50001,es"
    Then the response should return status code "200"
    And the value for "list" shouldn't be empty
    And the value for "city.name" should be "Zaragoza"
    And the value for "city.country" should be "ES"
    And the date for "list[1].dt_txt" should be LT than "list[2].dt_txt"
    And the value for "cnt" should be GT than 5

  Scenario: GET current weather forecast for a city by unexistent zip code
    Given I send a GET request to the service "forecast" with the parameters:
      | zip | 5247,es |
    Then the response should return status code "404"
    And the value for "message" should be "city not found"

  Scenario: GET current weather forecast for a city by wrong variable type zip code
    Given I send a GET request to the service "forecast" with the parameters:
      | zip | Valladolid,es |
    Then the response should return status code "404"
    And the value for "message" should be "city not found"

  Scenario: GET current weather forecast for a city by coords
    Given I send a GET request to the service "forecast" with the parameters:
      | lat | 41.66 |
      | lon | -0.88 |
    Then the response should return status code "200"
    And the value for "list" shouldn't be empty
    And the value for "city.name" should be "Zaragoza"
    And the value for "city.country" should be "ES"
    And the date for "list[1].dt_txt" should be LT than "list[2].dt_txt"
    And the value for "cnt" should be GT than 5

  Scenario: GET current weather forecast for a city by wrong coords
    Given I send a GET request to the service "forecast" with the parameters:
      | lon | -0.88 |
    Then the response should return status code "400"
    And the value for "message" should be "Nothing to geocode"
