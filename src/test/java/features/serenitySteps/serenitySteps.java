package features.serenitySteps;

import features.weatherCalls.servicesSupport;
import features.weatherCalls.weatherAPICalls;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static features.constants.AuthVariableKeys.RESPONSE_SESSION_VARIABLE;
import static features.matchers.RegexMatcher.matchesRegex;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class serenitySteps {

    private weatherAPICalls weatherAPICalls = new weatherAPICalls();
    private servicesSupport servicesSupport = new servicesSupport();

    @Step
    public void sendRequestCityWeather(String service, String city) {
        HashMap<String, String> parameters = new HashMap<>();
        if (city.matches("[0-9]{6,10}")) {
            parameters.put("id", city);
        }
        if (city.matches("[0-9]{5},[a-z]{2}")) {
            parameters.put("zip", city);
        }
        if (city.matches("[0-9]{5}")) {
            parameters.put("zip", city);
        }
        if (city.matches("[a-zA-Z]*.[a-zA-Z]*")) {
            parameters.put("q", city);
        }
        if (city.matches("[a-zA-Z]*")) {
            parameters.put("q", city);
        }
        weatherAPICalls.sendGetRequestCityTime(service, parameters);
    }

    @Step
    public void sendRequestCityWeather(String service, Map<String, String> parameters) {
        weatherAPICalls.sendGetRequestCityTime(service, parameters);
    }

    public void verifyStatus(int statusCode) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        servicesSupport.verifyStatusCode(statusCode, response.statusCode());

    }

    @Step
    public void verifyStatus(String statusCode) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        Assert.assertTrue("Matches",response.getBody().prettyPrint().matches(statusCode));
    }

    @Step
    public void verifyPathValue(String path, String value) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        response.then().body(path, matchesRegex(value));
    }

    @Step
    public void verifyPlainValue(String path, String value) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        response.then().body(path, matchesRegex(value));
    }

    @Step
    public void verifyPathValue(String path, Integer value) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        Assert.assertTrue("Integer matches", response.getBody().path(path).equals(value));
    }

    public void verifyPathValue(String path) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        response.then().body(path, nullValue());
    }

    public List<String> getListFromPath(String path) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        JsonPath jsonPath = response.jsonPath();
        return jsonPath.get(path);
    }

    public String getValueFromPath(String path) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        JsonPath jsonPath = response.jsonPath();
        return jsonPath.get(path);
    }

    @Step
    public void theValueForShouldBeThan(String path, String operation, long key) {
        long value = servicesSupport.getLongValue(path);

        switch (operation) {
            case "EQ":
                Assert.assertEquals(value, key);
                break;
            case "GT":
                Assert.assertTrue(value > key);
                break;
            case "GTE":
                Assert.assertTrue(value >= key);
                break;
            case "LT":
                Assert.assertTrue(value < key);
                break;
            case "LTE":
                Assert.assertTrue(value <= key);
                break;
            case "NEQ":
                Assert.assertNotEquals(value, key);
                break;
            default:
                Assert.fail("Specify a valid operation");
        }
    }

    @Step
    public void verifyPathValueIsNot(String path) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        response.then().body(path, is(not(nullValue())));
    }

    @Step
    public void verifyJsonArrayQuantity(String path, int quantity) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        Assert.assertThat(
                "Size not expected",
                response.path(path),
                hasSize(quantity)
        );
    }
}
