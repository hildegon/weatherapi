package features.serenityRunners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = {"src/test/resources/features"},
        plugin = {"pretty", "html:target/cucumber","json:target/cucumber-report.json"},
        glue = {"features.gherkinDefinitions"}
)

public class TestRunner {
}
