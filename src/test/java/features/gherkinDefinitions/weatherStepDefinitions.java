package features.gherkinDefinitions;


import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import features.serenitySteps.serenitySteps;
import net.serenitybdd.core.rest.RestMethod;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class weatherStepDefinitions {

    @Steps
    private serenitySteps serenitySteps = new serenitySteps();


    @Given("^I send a GET request to the service \"([^\"]*)\" to get the weather at the city \"([^\"]*)\"$")
    public void iSendAGETRequestToGetTheTimeAtTheCity(String service, String city) {
        serenitySteps.sendRequestCityWeather(service, city);
    }

    @Then("^the response should return status code \"(\\d+)\"$")
    public void theResponseShouldReturnStatusCode(int statusCode) {
        serenitySteps.verifyStatus(statusCode);
    }

    @And("^the value for \"(.*)\" should be \"(.*)\"$")
    public void theValueForShouldBe(String path, String value) {
        serenitySteps.verifyPathValue(path, value);
    }

    @And("^the value for \"(.*)\" should be equal than (.*)$")
    public void theValueForShouldBe(String path, Integer value) {
        serenitySteps.verifyPathValue(path, value);
    }

    @Given("^I send a GET request to the service \"([^\"]*)\" with the parameters:$")
    public void iSendAGETRequestToTheServiceWithTheParameters(String service, Map<String, String> parameters) {
        serenitySteps.sendRequestCityWeather(service, parameters);
    }

    @And("^the value for \"([^\"]*)\" (should|should not) contain the values:$")
    public void theValueForShouldContainTheValues(String path, String condition, List<String> valueNames) {
        List<String> actualValues = serenitySteps.getListFromPath(path);

        boolean shouldExist = "should".equals(condition);
        List<String> values = new ArrayList<>();
        values.addAll(valueNames);

        Assert.assertTrue(shouldExist ? actualValues.containsAll(values) : Collections.disjoint(actualValues, values));
    }

    @Then("^the value for \"([^\"]*)\" should be (EQ|GT|GTE|LT|LTE|NEQ) than (\\d+)$")
    public void theValueForShouldBeThan(String path, String operation, long key) {
        serenitySteps.theValueForShouldBeThan(path, operation, key);
    }

    @And("^the value for \"([^\"]*)\" (should|shouldn't) be empty$")
    public void theValueForIsEmpty(String path, String condition) {
        if ("should".equals(condition)) {
            serenitySteps.verifyPathValue(path);
        } else {
            serenitySteps.verifyPathValueIsNot(path);
        }
    }

    @Then("^the date for \"([^\"]*)\" should be (EQ|GT|GTE|LT|LTE|NEQ) than \"(.*)\"$")
    public void theValueForShouldBeThanDate(String path, String operation, String key) {
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        String date = serenitySteps.getValueFromPath(key);
        if (date.matches("\\d{4}.\\d{2}.\\d{2}.\\d{2}.\\d{2}.\\d{2}")) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }else if (date.matches("^(?:[1-9]\\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\\d|2[0-3]):[0-5]\\d:[0-5]\\d\\.\\d{0,4}(?:Z|(?:-|\\+)[01]\\d[0-5]\\d)")){
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        }else if(date.matches("^(?:[1-9]\\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\\d|2[0-3]):[0-5]\\d:[0-5]\\d(?:Z|(?:-|\\+)[01]\\d[0-5]\\d)")){
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        }
        Date parsedTimeStamp = null;
        try {
            parsedTimeStamp = dateFormat.parse(date);
        } catch (ParseException e) {
            Assert.fail("Unable to parse date: " + e);
        }
        serenitySteps.theValueForShouldBeThan(path, operation, (parsedTimeStamp).getTime());
    }

    @And("^the JSON array located on \"(.*)\" should contain (\\d+) items$")
    public void theJSONArrayLocatedOnShouldContainItems(String path, int quantity) {
        serenitySteps.verifyJsonArrayQuantity(path, quantity);
    }


    @Then("^the response should return code \"(.*)\"$")
    public void theResponseShouldReturnStatusCode(String statusCode) {
        serenitySteps.verifyStatus(statusCode);
    }
}
