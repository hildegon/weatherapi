package features.weatherCalls;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;

import java.util.HashMap;
import java.util.Map;

import static features.constants.AuthVariableKeys.*;
import static net.serenitybdd.rest.SerenityRest.rest;

public class weatherAPICalls {

    public Response sendGetRequestCityTime(String service, Map<String, String> parameters){
        RequestSpecification specification = rest()
                .header(X_API_KEY, API_KEY_CODE)
                .relaxedHTTPSValidation()
                .params(parameters);

        Response response =specification.when().get(OPENWEATHER_URL+service)
                .then()
                .extract()
                .response();
        Serenity.setSessionVariable(RESPONSE_SESSION_VARIABLE).to(response);

        return response;
    }

}
