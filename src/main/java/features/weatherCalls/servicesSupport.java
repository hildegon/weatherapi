package features.weatherCalls;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static features.constants.AuthVariableKeys.RESPONSE_SESSION_VARIABLE;

public class servicesSupport {

    public void verifyStatusCode(int expectedStatusCode, int currentStatusCode) {
        Assert.assertEquals("Status code must be: " + expectedStatusCode, expectedStatusCode, currentStatusCode);
    }

    public static void verifyStatusCode(String expectedStatusCode, String currentStatusCode) {
        Assert.assertEquals("Status code must be: " + expectedStatusCode, expectedStatusCode, currentStatusCode);
    }

    public long getLongValue(String path) {
        Object value = getValue(path);
        if (value instanceof Number)
            return ((Number) value).longValue();
        if (value.toString().matches("\\d{4}.\\d{2}.\\d{2}.\\d{2}.\\d{2}.\\d{2}") ){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date parsedTimeStamp = null;
            try {
                parsedTimeStamp = dateFormat.parse((String)value);
            } catch (ParseException e) {
                Assert.fail("Date couldn't be parsed: " + e);
            }
            return parsedTimeStamp.getTime();
        } else if (value.toString().matches("^(?:[1-9]\\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\\d|2[0-3]):[0-5]\\d:[0-5].*") ){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date parsedTimeStamp = null;
            try {
                parsedTimeStamp = dateFormat.parse((String)value);
            } catch (ParseException e) {
                Assert.fail("Date couldn't be parsed: " + e);
            }
            return parsedTimeStamp.getTime();
        }
        else
            return Long.parseLong(value.toString());
    }

    @Step
    public Object getValue(String parameter) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        return response.path(parameter);
    }
}
