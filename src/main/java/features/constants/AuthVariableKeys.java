package features.constants;

public class AuthVariableKeys {

    public static final String X_API_KEY = "x-api-key";
    public static final String API_KEY_CODE = "d8ab7f74f1375f0aceae177f121c4a04";
    public static final String OPENWEATHER_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String RESPONSE_SESSION_VARIABLE = "response";


    private AuthVariableKeys(){}

}
